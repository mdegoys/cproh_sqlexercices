# cproh_sqlexercices

**1. Clone the project, and install it (you need Node.js/npm)**

run `npm install`

**2. Install the user admin for mysql (you need mysql), and grant him all privileges**

run `sudo mysql -u root -p`

then 


`CREATE USER 'admin'@'localhost' IDENTIFIED BY '=@!#254tecmint';`

`GRANT ALL PRIVILEGES ON *.* TO 'admin'@'localhost' WITH GRANT OPTION;`

`FLUSH PRIVILEGES;`

**3. Run the code**


run `node exo.js`