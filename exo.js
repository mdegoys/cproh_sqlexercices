var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'admin',
  password : '=@!#254tecmint',
	multipleStatements: true
  //database : 'cproh'
});
 
connection.connect();
 
//connection.query('SELECT 1 + 1 AS solution', function (error, results, fields) {
//  if (error) throw error;
//  console.log('The solution is: ', results[0].solution);
//});
 
// Part One 
connection.query('CREATE DATABASE IF NOT EXISTS cproh', function (error, results, fields) {
  if (error) throw error;
  console.log('Database cproh created');
});

connection.query('CREATE TABLE IF NOT EXISTS cproh.users (	id int NOT NULL,	firstname varchar(255) NOT NULL,	lastname varchar(255) NOT NULL,	email varchar(255) NOT NULL, age int,	companyName varchar(50) NOT NULL, UNIQUE(id))',
function (error, results, fields) {
  if (error) throw error;
  console.log('Table users created');
});

connection.query('INSERT INTO cproh.users VALUES (1, "Matthieu", "de Goys", "mdegoys@gmail.com", 32, "Appaloosa") ON DUPLICATE KEY UPDATE id = 1; INSERT INTO cproh.users VALUES (2, "Mike", "Mikee", "mike@mikee.com", 24, "Simplon prod") ON DUPLICATE KEY UPDATE id = 2;',
function (error, results, fields) {
  if (error) throw error;
  console.log('Users created');
});

connection.query('SELECT email FROM cproh.users ORDER BY age ASC',
function (error, results, fields) {
  if (error) throw error;
  console.log(results);
});

// Part Two
connection.query('CREATE DATABASE IF NOT EXISTS nba', function (error, results, fields) {
  if (error) throw error;
  console.log('Database nba created');
});

connection.query('CREATE TABLE IF NOT EXISTS nba.players (	playerID int NOT NULL,	firstname varchar(255) NOT NULL,	lastname varchar(255) NOT NULL,	number int,	position varchar(255) NOT NULL,	height int,	age int, teamID varchar(50) NOT NULL, UNIQUE(playerID))',
function (error, results, fields) {
  if (error) throw error;
  console.log('Table players created');
});

connection.query('CREATE TABLE IF NOT EXISTS nba.teams (	teamID varchar(50) NOT NULL,	name varchar(255) NOT NULL, city varchar(255), state varchar(255), division varchar(255), UNIQUE(teamID))',
function (error, results, fields) {
  if (error) throw error;
  console.log('Table teams created');
});
 
connection.query('CREATE TABLE IF NOT EXISTS nba.stats ( teamID varchar(50) NOT NULL,	playerID int NOT NULL, pointsAverage int, minutesAverage int, fieldGoalPercentage int, threePointPercentage int, freeThrowPercentage int, UNIQUE(teamID))',
function (error, results, fields) {
  if (error) throw error;
  console.log('Table stats created');
});

connection.query('INSERT INTO nba.players VALUES (1, "Derrick", "Rose", 23, "G", 190, 30, "MIN") ON DUPLICATE KEY UPDATE playerID = 1; INSERT INTO nba.players VALUES (2, "Kevin", "Durant", 35, "SF", 206, 30, "GSW") ON DUPLICATE KEY UPDATE playerID = 2; INSERT INTO nba.players VALUES (3, "LeBron", "James", 23, "PF", 203, 34, "LAL") ON DUPLICATE KEY UPDATE playerID = 3; INSERT INTO nba.players VALUES (4, "Stephen", "Curry", 23, "G", 191, 30, "GSW") ON DUPLICATE KEY UPDATE playerID = 4;',
function (error, results, fields) {
  if (error) throw error;
  console.log('Players created');
});

connection.end();
